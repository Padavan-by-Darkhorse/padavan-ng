/* use tcp wrapper */
/* #undef HAVE_LIBWRAP */

/* Name of package */
#define PACKAGE "usbip-utils"

/* Define to the full name of this package. */
#define PACKAGE_NAME "usbip-utils"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "usbip-utils 1.1.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "usbip-utils"

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.1.1"

/* binary-coded decimal version number */
#define USBIP_VERSION 0x00000111

/* Version number of package */
#define VERSION "1.1.1"
